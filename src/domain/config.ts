class Config {
  static get NODE_ENV() {
    return process.env.NODE_ENV;
  }
}

export default Config;
